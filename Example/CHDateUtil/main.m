//
//  main.m
//  CHDateUtil
//
//  Created by coderdc on 02/09/2018.
//  Copyright (c) 2018 coderdc. All rights reserved.
//

@import UIKit;
#import "CHAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CHAppDelegate class]));
    }
}
