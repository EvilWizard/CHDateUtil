//
//  CHViewController.m
//  CHDateUtil
//
//  Created by coderdc on 02/09/2018.
//  Copyright (c) 2018 coderdc. All rights reserved.
//

#import "CHViewController.h"
#import "CHDateUtil.h"

@interface CHViewController ()

@end

@implementation CHViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSString *s = @"1990.01.01 19:12";
    NSArray *tmpArr = [CHDateUtil componentsWith:s withSourceFormat:@"yyyy.MM.dd HH:mm"];
    NSLog(@"%@",tmpArr);
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
