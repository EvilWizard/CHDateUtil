//
//  CHDateUtil.m
//  CHDateUtil
//
//  Created by 行者栖处 on 2017/11/8.
//  Copyright © 2017年 dc. All rights reserved.

#import "CHDateUtil.h"

@implementation CHDateUtil
+ (NSDateFormatter *)dateFormatWith:(NSString *)format {
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:format];
    [dateFormat setLocale:[NSLocale localeWithLocaleIdentifier:@"zh-CN"]];
    [dateFormat setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    return dateFormat;
}

+ (NSDate *)dateFromSource:(NSString *)string
          withSourceFormat:(NSString *)sourceFormat {
    
    NSDateFormatter *format = [CHDateUtil dateFormatWith:sourceFormat];
    NSDate *target = [format dateFromString:string];
    target = [target dateByAddingTimeInterval:-3600 *8];
    return target;
}

+ (NSString *)stringFromSourceDate:(NSDate *)sourceDate
                      targetFormat:(NSString *)targetFormat {
    
    NSDateFormatter *targetF = [CHDateUtil dateFormatWith:targetFormat];
    NSString *target = [targetF stringFromDate:sourceDate];
    return target;
}

+ (NSString *)covertDateString:(NSString *)sourceString
              withSourceFormat:(NSString *)sourceFormat
                toTargetFormat:(NSString *)targetFormat {
    
    NSDateFormatter *sourceF = [CHDateUtil dateFormatWith:sourceFormat];
    NSDateFormatter *targetF = [CHDateUtil dateFormatWith:targetFormat];
    NSDate *date = [sourceF dateFromString:sourceString];
    NSString *target = [targetF stringFromDate:date];
    return target;
}

+ (NSArray *)componentsWith:(NSString *)sourceString withSourceFormat:(NSString *)format {
    NSString *year = @"";
    NSString *month = @"";
    NSString *day = @"";
    NSDate *date = [CHDateUtil dateFromSource:sourceString withSourceFormat:format];
    NSCalendar *calendar = [NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitWeekday fromDate:date];
    year = [NSString stringWithFormat:@"%ld年",components.year];
    month = [NSString stringWithFormat:@"%02ld月",components.month];
    day = [NSString stringWithFormat:@"%02ld日",components.day];
    NSMutableArray *tmpArray = [NSMutableArray array];
    if ([format containsString:@"y"]) {
        [tmpArray addObject:year];
    }
    if ([format containsString:@"M"]) {
        [tmpArray addObject:month];
    }
    if ([format containsString:@"d"]) {
        [tmpArray addObject:day];
    }
    return tmpArray.copy;
}
@end
