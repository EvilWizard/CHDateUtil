//
//  CHDateUtil.h
//  CHDateUtil
//
//  Created by 行者栖处 on 2017/11/8.
//  Copyright © 2017年 dc. All rights reserved.

/// 1. 获取日期格式化器 dateFormatWith:
/// 2. 将日期字符串转化为NSDate类型 dateFromSource: withSourceFormat:
/// 3. 将NSDate类型指定格式化器转化为日期字符串 stringFromSourceDate: targetFormat:
/// 4. 将源日期字符串转化为指定格式的目标日期字符串 covertDateString: withSourceFormat: toTargetFormat:

#import <Foundation/Foundation.h>

@interface CHDateUtil : NSObject

/**
 返回一个日期格式化器
 
 @param format 日期格式化样式
 @return 日期格式化器
 */
+ (NSDateFormatter *)dateFormatWith:(NSString *)format;

/**
 将目标日期字符串转化为NSDate类型
 
 @param string 目标日期字符串
 @param sourceFormat 目标日期字符串的格式化器字符串
 @return 目标日期字符串的NSDate
 */
+ (NSDate *)dateFromSource:(NSString *)string
          withSourceFormat:(NSString *)sourceFormat;

/**
 将源日期转化为目标格式化器样式字符串
 
 @param sourceDate 源日期
 @param targetFormat 目标格式化器字符串
 @return 目标字符串
 */
+ (NSString *)stringFromSourceDate:(NSDate *)sourceDate
                      targetFormat:(NSString *)targetFormat;

/**
 将源日期字符串转化为目标格式化器形式的日期字符串
 
 @param sourceString 源日期字符串
 @param sourceFormat 源格式化器字符串
 @param targetFormat 目标格式化器字符串
 @return 目标字符串
 */
+ (NSString *)covertDateString:(NSString *)sourceString
              withSourceFormat:(NSString *)sourceFormat
                toTargetFormat:(NSString *)targetFormat;

/**
 将来源日期字符串拆分成日期字符串数组([2018年,01月,01日])
 
 @param sourceString 源日期字符串
 @param format 源日期字符串格式化器字符串
 @return 目标数组
 */
+ (NSArray *)componentsWith:(NSString *)sourceString
           withSourceFormat:(NSString *)format;
@end

