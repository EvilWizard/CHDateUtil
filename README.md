# CHDateUtil

[![CI Status](http://img.shields.io/travis/coderdc/CHDateUtil.svg?style=flat)](https://travis-ci.org/coderdc/CHDateUtil)
[![Version](https://img.shields.io/cocoapods/v/CHDateUtil.svg?style=flat)](http://cocoapods.org/pods/CHDateUtil)
[![License](https://img.shields.io/cocoapods/l/CHDateUtil.svg?style=flat)](http://cocoapods.org/pods/CHDateUtil)
[![Platform](https://img.shields.io/cocoapods/p/CHDateUtil.svg?style=flat)](http://cocoapods.org/pods/CHDateUtil)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

CHDateUtil is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'CHDateUtil'
```

## Author

EvilWizard, duanchao19900812@gmail.com

## License

CHDateUtil is available under the MIT license. See the LICENSE file for more info.
