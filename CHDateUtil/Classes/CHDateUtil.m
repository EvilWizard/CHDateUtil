//
//  CHDateUtil.m
//  CHDateUtil
//
//  Created by 行者栖处 on 2017/11/8.
//  Copyright © 2017年 dc. All rights reserved.

#import "CHDateUtil.h"

#define CH_ISNOTEMPTY_STRING(s) (s && [s isKindOfClass:[NSString class]] && ![s isEqualToString:@""] && ![s isEqual:[NSNull null]])

@implementation CHDateUtil

+ (NSCalendar *)currentCalendar {
    NSCalendar *calendar = [NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian];
    [calendar setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    return calendar;
}

+ (NSDateFormatter *)dateFormatWith:(NSString *)format {
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:format];
    [dateFormat setLocale:[NSLocale localeWithLocaleIdentifier:@"zh-CN"]];
    [dateFormat setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    // 设置默认公历 
    [dateFormat setCalendar:[self currentCalendar]];
    return dateFormat;
}

+ (NSDate *)dateFromSource:(NSString *)string
          withSourceFormat:(NSString *)sourceFormat {
    
    NSDateFormatter *format = [CHDateUtil dateFormatWith:sourceFormat];
    NSDate *target = [format dateFromString:string];
    return target;
}

+ (NSString *)stringFromSourceDate:(NSDate *)sourceDate
                      targetFormat:(NSString *)targetFormat {
    
    NSDateFormatter *targetF = [CHDateUtil dateFormatWith:targetFormat];
    NSString *target = [targetF stringFromDate:sourceDate];
    return target;
}

+ (NSString *)covertDateString:(NSString *)sourceString
              withSourceFormat:(NSString *)sourceFormat
                toTargetFormat:(NSString *)targetFormat {
    
    NSDateFormatter *sourceF = [CHDateUtil dateFormatWith:sourceFormat];
    NSDateFormatter *targetF = [CHDateUtil dateFormatWith:targetFormat];
    NSDate *date = [sourceF dateFromString:sourceString];
    NSString *target = [targetF stringFromDate:date];
    return target;
}

+ (NSArray *)componentsWith:(NSString *)sourceString withSourceFormat:(NSString *)format {
    NSString *year = @"";
    NSString *month = @"";
    NSString *day = @"";
    NSString *hour = @"";
    NSString *minute = @"";
    NSString *second = @"";
    NSDate *date = [CHDateUtil dateFromSource:sourceString withSourceFormat:format];
    NSCalendar *calendar = [NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian];
    calendar.timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    NSDateComponents *components = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitWeekday | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond fromDate:date];
    year = [NSString stringWithFormat:@"%ld年",(long)components.year];
    month = [NSString stringWithFormat:@"%02ld月",(long)components.month];
    day = [NSString stringWithFormat:@"%02ld日",(long)components.day];
    hour = [NSString stringWithFormat:@"%02ld时",(long)components.hour];
    minute = [NSString stringWithFormat:@"%02ld分",(long)components.minute];
    second = [NSString stringWithFormat:@"%02ld秒",(long)components.second];
    NSMutableArray *tmpArray = [NSMutableArray array];
    if ([format containsString:@"y"]) {
        [tmpArray addObject:year];
    }
    if ([format containsString:@"M"]) {
        [tmpArray addObject:month];
    }
    if ([format containsString:@"d"]) {
        [tmpArray addObject:day];
    }
    if ([format containsString:@"H"]) {
        [tmpArray addObject:hour];
    }
    if ([format containsString:@"m"]) {
        [tmpArray addObject:minute];
    }
    if ([format containsString:@"s"]) {
        [tmpArray addObject:second];
    }
    return tmpArray.copy;
}

+ (NSString *)todayDateWithFormat:(NSString *)targetFormat {
    return [CHDateUtil stringFromSourceDate:[NSDate dateWithTimeIntervalSinceNow:3600 *8] targetFormat:targetFormat];
}

+ (NSString *)yesterdayDateWithFormat:(NSString *)targetFormat {
    NSDate *date = [NSDate dateWithTimeIntervalSinceNow:3600 *(8 - 24)];
    return [CHDateUtil stringFromSourceDate:date targetFormat:targetFormat];
}

+ (NSString *)tomorrowDateWithFormat:(NSString *)targetFormat {
    NSDate *date = [NSDate dateWithTimeIntervalSinceNow:3600 *(8 + 24)];
    return [CHDateUtil stringFromSourceDate:date targetFormat:targetFormat];
}

+ (NSString *)weekdayWithDate:(NSString *)dateStr
                 sourceFormat:(NSString *)sourceFormat
                 targetFormat:(NSString *)targetFormat {
    NSDate *date = [CHDateUtil dateFromSource:dateStr withSourceFormat:sourceFormat];
    NSDateComponents *components = [[self currentCalendar] components:NSCalendarUnitWeekday fromDate:date];
    NSInteger index = components.weekday - 2;
    if (components.weekday == 1) {
        index = 6;
    }
    NSDate *monday = [date dateByAddingTimeInterval:(3600 * 24 *(-index))];
    NSDate *sunday = [date dateByAddingTimeInterval:(3600 * 24) * (7 - index - 1)];
    NSString *mon = [self stringFromSourceDate:monday targetFormat:targetFormat];
    NSString *sun = [self stringFromSourceDate:sunday targetFormat:targetFormat];
    return [NSString stringWithFormat:@"%@-%@",mon,sun];
}

+ (NSString *)currentWeekWithFormat:(NSString *)format {
    NSString *today = [self todayDateWithFormat:@"yyyy.MM.dd"];
    return [self weekdayWithDate:today sourceFormat:@"yyyy.MM.dd" targetFormat:format];
}

+ (NSString *)lastWeekWithFormat:(NSString *)format {
    NSDate *today = [NSDate dateWithTimeIntervalSinceNow:3600 *8];
    NSDate *lastWeekDay = [today dateByAddingTimeInterval:- 3600 *24 *7];
    NSString *date = [self stringFromSourceDate:lastWeekDay targetFormat:@"yyyy.MM.dd"];
    return [self weekdayWithDate:date sourceFormat:@"yyyy.MM.dd" targetFormat:format];
}

+ (NSString *)nextWeekWithFormat:(NSString *)format {
    NSDate *today = [NSDate dateWithTimeIntervalSinceNow:3600 *8];
    NSDate *nextWeekDay = [today dateByAddingTimeInterval:3600 *24 *7];
    NSString *date = [self stringFromSourceDate:nextWeekDay targetFormat:@"yyyy.MM.dd"];
    return [self weekdayWithDate:date sourceFormat:@"yyyy.MM.dd" targetFormat:format];
}

+ (NSArray <NSString *>*)monthDateIntervalWithMonth:(NSString *)monthStr
                                       sourceFormat:(NSString *)sourceFormat
                                       targetFormat:(NSString *)targetFormat {
    NSDate *date = [self dateFromSource:monthStr withSourceFormat:sourceFormat];
    NSRange range = [[self currentCalendar] rangeOfUnit:NSCalendarUnitDay inUnit:NSCalendarUnitMonth forDate:date];
    NSString *firstDay = [self stringFromSourceDate:date targetFormat:@"yyyyMMdd"];
    NSDate *firstDate = [self dateFromSource:firstDay withSourceFormat:@"yyyyMMdd"];
    NSDate *lastDate = [firstDate dateByAddingTimeInterval:3600 *24 * (range.length - 1)];
    NSString *lastDay = [self stringFromSourceDate:lastDate targetFormat:@"yyyyMMdd"];
    firstDay = CH_ISNOTEMPTY_STRING(firstDay) ? firstDay : @"";
    lastDay = CH_ISNOTEMPTY_STRING(lastDay) ? lastDay : @"";
    return @[firstDay,lastDay];
}

+ (NSString *)monthWithDay:(NSString *)day sourceFormat:(NSString *)sourceFormat targetFormat:(NSString *)targetFormat {
    NSDate *date = [self dateFromSource:day withSourceFormat:sourceFormat];
    return [self stringFromSourceDate:date targetFormat:targetFormat];
}

+ (NSString *)currentMonthWithFormat:(NSString *)format {
    NSDate *date = [NSDate dateWithTimeIntervalSinceNow:3600 *8];
    return [self stringFromSourceDate:date targetFormat:format];
}

+ (NSString *)lastMonthWithFormat:(NSString *)format {
    return [self monthIntervalWith:-1 sourceDate:[self todayDateWithFormat:format] sourceformat:format targetFormat:format];
}

+ (NSString *)nextMonthWithFormat:(NSString *)format {
    return [self monthIntervalWith:1 sourceDate:[self todayDateWithFormat:format] sourceformat:format targetFormat:format];
}

+ (NSString *)monthIntervalWith:(NSInteger)interval sourceDate:(NSString *)sourceDate sourceformat:(NSString *)sourceFormat targetFormat:(NSString *)targetFormat {
    NSDate *date = [self dateFromSource:sourceDate withSourceFormat:sourceFormat];
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    [dateComponents setMonth:interval];
    NSDate *res = [[self currentCalendar] dateByAddingComponents:dateComponents toDate:date options:0];
    return [self stringFromSourceDate:res targetFormat:targetFormat];
}

+ (NSString *)timeIntervalWithYear:(NSInteger)year month:(NSInteger)month day:(NSInteger)day sourceDate:(NSString *)sourceDate sourceFormat:(NSString *)sourceFormat targetFormat:(NSString *)targetFormat {
    NSDate *date = [self dateFromSource:sourceDate withSourceFormat:sourceFormat];
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    [dateComponents setYear:year];
    [dateComponents setMonth:month];
    [dateComponents setDay:day];
    NSDate *res = [[self currentCalendar] dateByAddingComponents:dateComponents toDate:date options:0];
    return [self stringFromSourceDate:res targetFormat:targetFormat];
}

+ (NSString *)timeIntervalWithComponents:(NSDateComponents *)comps sourceDate:(NSString *)sourceDate sourceFormat:(NSString *)sourceFormat targetFormat:(NSString *)targetFormat {
    NSDate *date = [self dateFromSource:sourceDate withSourceFormat:sourceFormat];
    NSDate *res = [[self currentCalendar] dateByAddingComponents:comps toDate:date options:0];
    return [self stringFromSourceDate:res targetFormat:targetFormat];
}

+ (NSComparisonResult)dateStr1:(NSString *)dateStr1 compareDateStr2:(NSString *)dateStr2 sourceFormat:(NSString *)format {
    NSDate *date1 = [self dateFromSource:dateStr1 withSourceFormat:format];
    NSDate *date2 = [self dateFromSource:dateStr2 withSourceFormat:format];
    if ([date1 timeIntervalSinceDate:date2] == 0) {
        return NSOrderedSame;
    }
    else if ([date1 timeIntervalSinceDate:date2] > 0) {
        return NSOrderedDescending;
    }
    else {
        return NSOrderedAscending;
    }
}

+ (NSArray <NSString *>*)weekDayWithYear:(NSInteger)year weekIndex:(NSInteger)weekIndex targetFormat:(NSString *)targetFormat {
    //获得了时间轴
    NSDate *date = [NSDate date];
    
    //日历类 提供大部分的时间计算接口
    NSCalendar *calendar = [self currentCalendar];
    /**这两个参数的设置影响着周次的个数和划分*****************/
    [calendar setFirstWeekday:2]; //设置每周的开始是星期一
    [calendar setMinimumDaysInFirstWeek:7]; //设置一周至少需要几天
    /****************/
    //一个封装了具体年月日、时秒分、周、季度等的类
    NSDateComponents *comps = [calendar components:(NSCalendarUnitWeekOfYear | NSCalendarUnitWeekday | NSCalendarUnitWeekdayOrdinal | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay)
                                          fromDate:date];
    
    //时间轴是当前年的第几周
    NSInteger todayIsWeek = [comps weekOfYear];
    NSInteger todayIsWeekDay = [comps weekday];
    
    long firstDiff,lastDiff;
    if (todayIsWeekDay == 1) {
        firstDiff = -6;
        lastDiff = 0;
    }else
    {
        firstDiff = [calendar firstWeekday] - todayIsWeekDay;
        lastDiff = 8 - todayIsWeekDay;
    }
    
    NSDate *firstDayOfWeek= [NSDate dateWithTimeInterval:24*60*60*firstDiff sinceDate:date];
    NSDate *lastDayOfWeek= [NSDate dateWithTimeInterval:24*60*60*lastDiff sinceDate:date];
    
    long weekdifference = weekIndex - todayIsWeek;
    
    firstDayOfWeek= [NSDate dateWithTimeInterval:24*60*60*7*weekdifference sinceDate:firstDayOfWeek];
    lastDayOfWeek= [NSDate dateWithTimeInterval:24*60*60*7*weekdifference sinceDate:lastDayOfWeek];
    
    NSDateComponents *firstDayOfWeekcomps = [calendar components:(NSCalendarUnitWeekOfYear | NSCalendarUnitWeekday | NSCalendarUnitWeekdayOrdinal | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:firstDayOfWeek];
    NSDateComponents *lastDayOfWeekcomps = [calendar components:(NSCalendarUnitWeekOfYear | NSCalendarUnitWeekday | NSCalendarUnitWeekdayOrdinal | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:lastDayOfWeek];
    
    NSDate *monday = [calendar dateFromComponents:firstDayOfWeekcomps];
    NSDate *sunday = [calendar dateFromComponents:lastDayOfWeekcomps];
    
    return @[[self stringFromSourceDate:monday targetFormat:targetFormat],
             [self stringFromSourceDate:sunday targetFormat:targetFormat]];
    
}

#pragma mark - 秒转化为日时分秒
+ (NSDateComponents *)componentsWithSeconds:(NSTimeInterval)seconds {
    NSTimeInterval dayDivisior = 60 * 60 * 24;
    NSTimeInterval hourDivisior = 60 *60;
    NSTimeInterval minuteDivisior = 60;
    NSInteger day = (NSInteger)seconds / dayDivisior;
    NSInteger hour = (NSInteger)(seconds - day *dayDivisior) / hourDivisior;
    NSInteger minute = (seconds - day *dayDivisior - hour *hourDivisior) / minuteDivisior;
    NSInteger second = (seconds - day *dayDivisior - hour *hourDivisior - minute *minuteDivisior);
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    dateComponents.day = day;
    dateComponents.hour = hour;
    dateComponents.minute = minute;
    dateComponents.second = second;
    return dateComponents;
}

+ (NSString *)timeIntervalWithSeconds:(NSTimeInterval)seconds {
    NSDateComponents *dateComponents = [self componentsWithSeconds:seconds];
    NSMutableString *s = [NSMutableString string];
    if (dateComponents.hour > 0) {
        [s appendFormat:@"%ld时",(long)dateComponents.hour];
    }
    if (dateComponents.minute > 0) {
        [s appendFormat:@"%02ld分",(long)dateComponents.minute];
    }
    if (dateComponents.second > 0) {
        [s appendFormat:@"%02ld秒",(long)dateComponents.second];
    }
    return s.copy;
}

@end
