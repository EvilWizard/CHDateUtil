
Pod::Spec.new do |s|
  s.name             = 'CHDateUtil'
  s.version          = '0.1.5'
  s.summary          = 'Date Format.'

  s.description      = <<-DESC
Date Format Utils
                       DESC

  s.homepage         = 'https://gitlab.com/EvilWizard/CHDateUtil.git'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'EvilWizard' => 'duanchao19900812@gmail.com' }
  s.source           = { :git => 'https://gitlab.com/EvilWizard/CHDateUtil.git', :tag => s.version.to_s }

  s.ios.deployment_target = '8.0'

  s.source_files = 'CHDateUtil/Classes/*'

end
